﻿# Emulace infrastrukturní jednotky pro systém inteligentní dopravy

Oficiálne repo mojej diplomovej práce (text + prezentácia)

## Diplomová práca

[![pipeline status](https://gitlab.com/juris/EmulaciaRSUpreITS/badges/master_theses/pipeline.svg)](https://gitlab.com/juris/EmulaciaRSUpreITS/commits/master)
[![download](https://img.shields.io/badge/Download-FAIL-red.svg)](https://gitlab.com/juris/test-ci/-/jobs/artifacts/semestral_theses/download?job=build_master_theses)

- [x] Odovzdanie diplomovej práce 21.05.2018 20:00 - elektronicka verze
- [x] Odovzdanie diplomovej práce 23.05.2018 10:30 - listinné verze, SC7.73
- [x] Obhajoba diplomovej práce 06.06.2018

### Zadanie

Nastudujte a popište technologii pro bezdrátovou komunikaci typu vozidlo-vozidlo a vozidlo-infrastruktura na bázi
ETSI ITS-G5 z hlediska standardizace i probíhajících testovacích provozů v EU. Srovnejte ji se standardy IEEE
1609.x DSRC/WAVE a situací v USA. V detailu se zaměřte na světelné křižovatky, výstražné značení a popište
protokol výměny příslušných zpráv SPaT, MAP a RSA dle existujících standardů. Implementuje aplikaci pro
emulaci infrastrukturního bodu na vývojovém kitu firmy ALPS Electric obsahujícím V2X rádiový modul
a vestavěný systém s Linux OS. Ověřte na aplikační úrovni funkčnost systému při testu s druhým kitem
s testovací aplikací reprezentující vozidlo.

#### Originální zadání závěrečné práce

- [x] Originální zadání bude k vyzvednutí na sekretariátu Ústavu telekomunikací Technická 12, Brno, 7.NP, dveře č. SC7.73 ve dnech 2. – 4. 5. vždy od 8:00 do 10:30.

## Semestrálna práca

[![pipeline status](https://gitlab.com/juris/EmulaciaRSUpreITS/badges/semestral_theses/pipeline.svg)](https://gitlab.com/juris/EmulaciaRSUpreITS/commits/master) 
[![download](https://img.shields.io/badge/Download-OK-brightgreen.svg)](https://gitlab.com/juris/test-ci/-/jobs/artifacts/semestral_theses/download?job=build_semestral_theses)

- [x] Odovzdanie semestrálnej práce 14.12.2017
- [x] Obhajoba semestrálnej práce 18.12.2017

### Zadanie

Nastudujte a popište technologii pro bezdrátovou komunikaci typu vozidlo-vozidlo a vozidlo-infrastruktura na bázi ETSI ITS-G5 z hlediska standardizace i probíhajících testovacích provozů v EU. Srovnejte ji se standardy IEEE 1609.x DSRC/WAVE a situací v USA. V detailu se zaměřte na světelné křižovatky, výstražné značení a popište protokol výměny příslušných zpráv SPaT, MAP a RSA dle existujících standardů. Navrhněte koncept aplikace, která umožní uživateli nadefinovat data včetně geometrie dopravní situace pro emulaci infrastrukturního bodu typu dopravní výstraha a světelná křižovatka, tyto data pak zpracovat a vysílat dle příslušných standardů.

## Preklad do PDF

pre Linux:

```
sudo apt install texlive-full latexmk
# build text
./build_tex.sh
```